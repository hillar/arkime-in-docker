#!/bin/sh
until curl -sS "http://$ES_HOST:$ES_PORT/_cluster/health?wait_for_status=yellow"
do
    echo "Waiting for ES to start"
    sleep 3
done
echo

export ARKIME_ELASTICSEARCH=http://$ES_HOST:$ES_PORT

# create config.ini with Configure
if [ ! -f $ARKIMEDIR/etc/config.ini ]; then
  echo "running Configure"
  cd $ARKIMEDIR/etc/
	[ -f config.ini.sample ] || curl -s -C - -O "https://raw.githubusercontent.com/arkime/arkime/master/release/config.ini.sample"
  Configure
	echo INIT | $ARKIMEDIR/db/db.pl http://$ES_HOST:$ES_PORT init
  arkime_add_user.sh admin "Admin User" $ARKIME_PASSWORD --admin
  if [ ! -z $WISE_HOST ]; then
    echo '# wise conf' >> $ARKIMEDIR/etc/config.ini
    echo 'plugins=wise.so' >> $ARKIMEDIR/etc/config.ini
    echo "wiseHost=${WISE_HOST}:${WISE_PORT}" >> $ARKIMEDIR/etc/config.ini
  fi
else
	echo "using existing config.ini"
  echo
	grep -v "#" $ARKIMEDIR/etc/config.ini | grep .
	echo

fi

# INIT DB
if [ "$INITALIZEDB" = "true" ] ; then
	echo 'INIT .. '
	echo INIT | $ARKIMEDIR/db/db.pl http://$ES_HOST:$ES_PORT init
	arkime_add_user.sh admin "Admin User" $ARKIME_PASSWORD --admin
	echo "  user: admin"
        echo "  password: $ARKIME_PASSWORD"
fi

# WIPE DB
if [ "$WIPEDB" = "true" ]; then
        echo 'WIPE .. '
        echo WIPE | $ARKIMEDIR/db/db.pl http://$ES_HOST:$ES_PORT wipe
fi


if [ "$CAPTURE" = "on" ]
then
    echo "Start capture..."
    arkime-capture >> $ARKIMEDIR/logs/capture.log 2>&1 &
fi

if [ "$MONITOR" = "on" ]
then
    echo "Start monitoring $SPOOLDIR ..."
    arkime-capture -R $SPOOLDIR -m --delete --copy >> $ARKIMEDIR/logs/monitor.log 2>&1 &
fi

if [ "$VIEWER" = "on" ]
then
   echo "Starting viewer ..."
   /bin/sh -c 'cd $ARKIMEDIR/viewer && $ARKIMEDIR/bin/node viewer.js -c $ARKIMEDIR/etc/config.ini | grep -v 'eshealth.json'| tee -a $ARKIMEDIR/logs/viewer.log 2>&1'
fi
